﻿using System.Configuration;
using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;

namespace backup
{
    public static class BlobStorage
    {
        private static string BACKUP_BUCKET = ConfigurationManager.AppSettings["BackupBucket"];

        private static AmazonS3 GetClient()
        {
            return AWSClientFactory.CreateAmazonS3Client();
        }

        public static void UploadFile(string filepath)
        {
            var client = GetClient();

            using (FileStream fs = new FileStream(filepath, FileMode.Open))
            {
                string filename = Path.GetFileName(filepath);

                client.PutObject(new PutObjectRequest
                {
                    BucketName = BACKUP_BUCKET,
                    Key = filename,
                    InputStream = fs
                });
            }
        }
    }
}
